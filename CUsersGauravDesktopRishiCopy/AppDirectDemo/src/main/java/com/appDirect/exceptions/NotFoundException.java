package com.appDirect.exceptions;

/**
 * @author gauravagrawal
 *
 */
public class NotFoundException extends Exception {

	public NotFoundException(String message) {
		super(message);
	}

}
