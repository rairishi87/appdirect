package com.appDirect.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.appDirect.entity.Employee;
import com.appDirect.exceptions.NotFoundException;
import com.appDirect.service.EmployeeService;

/**
 * Created by rishi on 28/09/16.
 */
@RestController
@RequestMapping("/employee")
public class EmployeeController {

    @Autowired
    private EmployeeService employeeService;

    @RequestMapping(method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    @PreAuthorize(value="hasRole('admin')")
    public Employee createUser(@RequestBody Employee employee) {
        return employeeService.createEmployee(employee);
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
	@RequestMapping(value = "{empId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @PreAuthorize(value="hasRole('admin')")
    public ResponseEntity<?> findUserbyId(@PathVariable String empId) {
    	try {
    		return ResponseEntity.ok( employeeService.findByEmpId(empId) );
		} catch (NotFoundException e) {
			return new ResponseEntity(e.getMessage(), HttpStatus.NOT_FOUND);
		}
    }
    
}
