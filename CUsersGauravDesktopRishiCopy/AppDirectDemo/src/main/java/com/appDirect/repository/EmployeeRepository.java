package com.appDirect.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.appDirect.entity.Employee;

/**
 * Created by rishi on 28/09/16.
 */
@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Long> {

    Employee findByEmpId(String empId);

}
