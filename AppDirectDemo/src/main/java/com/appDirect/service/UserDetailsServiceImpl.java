package com.appDirect.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.appDirect.dto.UserDto;
import com.appDirect.entity.Employee;
import com.appDirect.exceptions.NotFoundException;

@Service
public class UserDetailsServiceImpl implements UserDetailsService{

	@Autowired
	private EmployeeService employeeService;

	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

		Employee employee = null;
		try {
			employee = employeeService.findByEmpId(username);
		} catch (NotFoundException e) {
			throw new UsernameNotFoundException("Username " + username + " not found");
		}
		return new UserDto(employee);
	}

}
