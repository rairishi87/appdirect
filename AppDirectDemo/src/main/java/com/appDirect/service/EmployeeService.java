package com.appDirect.service;

import java.util.List;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.appDirect.entity.Employee;
import com.appDirect.exceptions.NotFoundException;
import com.appDirect.repository.EmployeeRepository;

/**
 * Created by rishi on 28/09/16.
 */
@Service
@Transactional
public class EmployeeService {
	
    @Autowired
    private EmployeeRepository employeeRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    public Employee createEmployee(Employee employee) {
    	employee.setPassword(passwordEncoder.encode(employee.getPassword()));
        return employeeRepository.save(employee);
    }

    public Employee findByEmpId(String empId) throws NotFoundException {
    	Employee employee = employeeRepository.findByEmpId(empId);
    	if(Objects.isNull(employee)){
			throw new NotFoundException("Invalid Employee: "+ empId);
		}
    	return employee;
    }

    public List<Employee> findAllEmployees() {
        return employeeRepository.findAll();
    }
    
    public void save(List<Employee> employees) {
    	employeeRepository.save(employees);
	}
}
