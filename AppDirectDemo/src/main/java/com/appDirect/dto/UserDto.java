package com.appDirect.dto;

import java.util.Collection;
import java.util.Date;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;

import com.appDirect.entity.Employee;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@ToString
@NoArgsConstructor
public class UserDto implements UserDetails{
	
	private String username;
	private String password;
	private String emailId;
	private String empName;
	private Date lastPasswordReset;
	private boolean isActive;
	private Collection<? extends GrantedAuthority> authorities;
	
	public UserDto(Employee employee) {
		if(employee != null)
		{
			this.setUsername(employee.getEmpId());
			this.setPassword(employee.getPassword());
			this.setEmailId(employee.getEmail());
			this.setEmpName(employee.getName());
			this.setLastPasswordReset(employee.getLastPasswordReset());
			this.setActive(employee.isActive());
			this.setAuthorities(AuthorityUtils.commaSeparatedStringToAuthorityList(employee.getAuthorities()));
		}		
	}

	public Collection<? extends GrantedAuthority> getAuthorities() {
		return this.authorities;
	}

	public boolean isAccountNonExpired() {
		return true;
	}

	public boolean isAccountNonLocked() {
		return true;
	}

	public boolean isCredentialsNonExpired() {
		return true;
	}

	public boolean isEnabled() {
		return this.isActive;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	public Date getLastPasswordReset() {
		return lastPasswordReset;
	}

	public void setLastPasswordReset(Date lastPasswordReset) {
		this.lastPasswordReset = lastPasswordReset;
	}

	public boolean isActive() {
		return isActive;
	}

	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}

	public void setAuthorities(Collection<? extends GrantedAuthority> authorities) {
		this.authorities = authorities;
	}

}
